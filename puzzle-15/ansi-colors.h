#ifndef _ANSI_COLORS_H
#define _ANSI_COLORS_H

#define COLOR_RED     "\x1b[31m"
#define COLOR_GREEN   "\x1b[32m"
#define COLOR_BLUE    "\x1b[34m"
#define COLOR_RESET   "\x1b[0m"

#define BG_GREEN "\x1b[42m"
#define BG_YELLOW "\x1b[43m"
#define BG_PURPLE "\x1b[45m"
#define BG_RED "\x1b[41m"

#endif
