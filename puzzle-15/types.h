#ifndef _TYPES_H
#define _TYPES_H

#define Bool int
#define False 0
#define True 1

#define CELL_SIZE 10

enum Options
{
  OPTION_SELECT = 0,
  OPTION_EXIT = -1,
  OPTION_RESTART = -2,
};

enum MenuOption
{
  MENU_START = 1,
  MENU_LOAD_SAVED,
  MENU_EXIT
};

#define KEY_UP 91
#define KEY_DOWN 80
#define KEY_LEFT 75
#define KEY_RIGHT 27

#endif
