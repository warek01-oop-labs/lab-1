#ifndef _DRAW_H
#define _DRAW_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"
#include "structures.h"
#include "utils.h"

void drawXLine(int lenght)
{
  for (int i = 0; i < lenght; i++)
    putchar('#');
}

void newLine()
{
  putchar('\n');
}

void drawText(const char* text, int cellSize, Bool isSelected)
{
  putchar('#');
  for (int i = 0; i < cellSize; i++)
    putchar(' ');

  printf(isSelected ? BG_GREEN "%s" COLOR_RESET : "%s", text);

  for (int i = 0; i < cellSize * 3 - strlen(text); i++)
    putchar(' ');

  putchar('#');
  newLine();
}

void drawEmptyLine(int cellSize)
{
  putchar('#');
  for (int i = 0; i < cellSize * 4; i++)
    putchar(' ');
  putchar('#');
  putchar('\n');
}

void clearScreen()
{
  system("clear");
  printf(BG_PURPLE "PUZZLE 15" COLOR_RESET "\n");
  printf("Input " COLOR_BLUE "%d" COLOR_RESET " to click\n", OPTION_SELECT);
  printf("Input " COLOR_RED "%d" COLOR_RESET " to quit\n", OPTION_EXIT);
  printf("Input " COLOR_RED "%d" COLOR_RESET " to restart\n", OPTION_RESTART);
}

int drawMenu()
{
  int key = MENU_START;
  int option = key;

  while (True)
  {
    clearScreen();
    drawXLine(CELL_SIZE * 4 + 2);
    newLine();

    for (int i = 0; i < 5; i++)
      drawEmptyLine(CELL_SIZE);
      
    drawText("1) NEW_GAME", CELL_SIZE, key == MENU_START);
    drawText("2) LOAD SAVED", CELL_SIZE, key == MENU_LOAD_SAVED);
    drawText("3) EXIT", CELL_SIZE, key == MENU_EXIT);

    for (int i = 0; i < CELL_SIZE * 2 - 8; i++)
      drawEmptyLine(CELL_SIZE);

    drawXLine(CELL_SIZE * 4 + 2);
    newLine();

    key = getInput(); 
    switch(key)
    {
      case OPTION_SELECT:
        return option;
      case OPTION_EXIT:
        exit(0);
      break;
      default:
        option = key;
    }
  }

  return option;
}

Bool drawGame(int cells[4][4]) 
{
  if (isSorted(cells))
  {
    printf(BG_YELLOW "YOU WON!" COLOR_RESET);
    return False;
  }

  int key = -999;
  int option;

  while(True)
  {
    if (key > 0 && key < 16) 
    {
      int ax, ay, bx, by;
      getCellXY(0, cells, &ax, &ay);
      getCellXY(key, cells, &bx, &by);
      if (
        ((ax == bx + 1 || ax == bx - 1) && ay == by) 
        || (ay == by + 1 || ay == by - 1) && ax == bx
      )
      {
        cells[ax][ay] = key;
        cells[bx][by] = 0;
      }
    } 
    else if (key == OPTION_EXIT)
    {
      exit(0);
    } 
    else if (key == OPTION_RESTART) 
    {
      return True;
    }

    saveGame(cells);

    clearScreen();
    printf(BG_RED);
    drawXLine(38);
    printf(COLOR_RESET);
    newLine();

    for (int i = 0; i < 4; i++)
    {
      printf(BG_RED "#" COLOR_RESET);
      for (int j = 0; j < 36; j++)
      {
        printf(" ");
      }
      printf(BG_RED "#");
      newLine();
      printf("#" COLOR_RESET);

      for (int j = 0; j < 4; j++)
      {
        printf(
          (cells[i][j] != 0)
          ? " ******* " 
          : "         "
        );
      }
      printf(BG_RED "#");
      newLine();
      printf("#" COLOR_RESET);
      for (int j = 0; j < 4; j++)
      {
        if (cells[i][j] == 0)
        {
          printf("         ");
        } else
        {
          printf(
            (cells[i][j] < 10)
              ? " *  " COLOR_GREEN "%d" COLOR_RESET "  * " 
              : " * " COLOR_GREEN "%d" COLOR_RESET "  * " , cells[i][j]
          );
        }
      }
      printf(BG_RED "#");
      newLine();
      printf("#" COLOR_RESET);
      for (int j = 0; j < 4; j++)
      {
        printf(
          (cells[i][j] != 0)
          ? " ******* " 
          : "         "
        );
      }
      printf(BG_RED "#");
      newLine();
    }
    

    drawXLine(38);
    printf(COLOR_RESET);
    newLine();

    key = getInput();
  }

}

#endif
