#ifndef _PUZZLE_H
#define _PUZZLE_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "ansi-colors.h"
#include "types.h"
#include "draw.h"
#include "utils.h"

void start()
{
  srand(time(NULL));
  int gameRunnig = False;
  int option;
  int cells[4][4];
  int arr[16];
  
  option = drawMenu();

  switch (option)
  {
  case MENU_START:
  {
    Bool restart;

    do
    {
      for (int i = 0; i < 16; i++)
      arr[i] = i;

      shuffle(arr, 16);

      int temp = 0;
      for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
          cells[i][j] = arr[temp++];

      restart = drawGame(cells);
    } while (restart);

    drawGame(cells);
    break;
  }
  case MENU_LOAD_SAVED:
    loadGame(cells);
    drawGame(cells);
  break;
  case MENU_EXIT:
    exit(0);
    break;
  }
  
}

#endif
