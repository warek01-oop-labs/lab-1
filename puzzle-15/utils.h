#ifndef _UTILS_H
#define _UTILS_H

#include <stdio.h>
#include <stdlib.h>

#include "ansi-colors.h"
#include "types.h"

void getCellXY(int number, int cells[4][4], int* x, int* y)
{
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
      if (cells[i][j] == number)
      {
        *x = i;
        *y = j;
      }
}

void shuffle(int *array, int n)
{
  if (n > 1) 
  {
    int i;
    for (i = 0; i < n - 1; i++) 
    {
      int j = i + rand() / (RAND_MAX / (n - i) + 1);
      int t = array[j];
      array[j] = array[i];
      array[i] = t;
    }
  }
}

int getInput()
{
  int key;
  printf(COLOR_BLUE);
  scanf("%d", &key);
  printf(COLOR_RESET);
  return key;
}

int randRange(int from, int to)
{
  return rand() % (to + 1 - from) + from;
}

void saveGame(int cells[4][4])
{
  FILE* file = fopen("save", "w");

  for (int i = 0; i < 4; i++)
   for (int j = 0; j < 4; j++)
  {
    fprintf(file, "%d ", cells[i][j]);
  }
  fprintf(file, "\n");
  fclose(file);
}

void loadGame(int cells[4][4])
{
  FILE* file = fopen("save", "r");
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
    {
      int a;
      fscanf(file, "%d", &a);
      cells[i][j] = a;
    }

  fclose(file);
}

Bool isSorted(int cells[4][4])
{
  int prev = cells[0][0];
  for (int i = 0; i < 4; i++)
    for (int j = 0; j < 4; j++)
    {
      if (i == 0 && j == 0)
        continue;

      prev = (j == 0 && i > 0)
        ? cells[i - 1][3]
        : cells[i][j - 1];

      if (prev > cells[i][j])
        return False;
    }

  return True;
}

#endif
