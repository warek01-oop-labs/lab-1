#ifndef _STRUCTURES_H
#define _STRUCTURES_H

#include "types.h"

typedef struct PuzzleCell PuzzleCell;
typedef struct Coord Coord;

struct Coord
{
  int x;
  int y;
};

struct PuzzleCell
{
  Bool isSelected;
  int number;
  Coord coord;
};

#endif
