#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "LinkedList.h"

int main(int argc, char* argv[])
{
  LinkedList* list = linkedList_create();
  linkedList_append(list, 50);
  linkedList_append(list, 20);
  linkedList_append(list, 30);
  linkedList_append(list, 40);
  linkedList_append(list, 60);

  linkedList_sort(list);

  linkedList_print(list);

  return 0;
}
