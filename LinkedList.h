#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <stdlib.h>
#include <stdio.h>

#define BOOL int
#define FALSE 0
#define TRUE 1

typedef struct LinkedListNode LinkedListNode;

struct LinkedListNode
{
  int data;
  LinkedListNode* prev;
  LinkedListNode* next;
};

typedef struct LinkedList LinkedList;

struct LinkedList
{
  int size;
  LinkedListNode* head;
  LinkedListNode* tail;
};

LinkedListNode* linkedListNode_create(int data)
{
  LinkedListNode* newNode = (LinkedListNode*)malloc(sizeof(LinkedListNode));
  newNode->data = data;
  newNode->next = NULL;
  newNode->prev = NULL;
  return newNode;
}

void linkedListNode_print(LinkedListNode* node)
{
  printf("%d\n", node->data);
}

void linkedListNode_swap(LinkedListNode* a, LinkedListNode* b)
{
  LinkedListNode* tempNext = a->next;
  LinkedListNode* tempPrev = a->prev;

  if (a->prev)
  {
    a->prev->next = b;
  }

  if (a->next)
  {
    a->next->prev = b;
  }

  a->next = b->next;
  a->prev = b->prev;

  if (b->prev)
  {
    b->prev->next = a;
  }

  if (b->next)
  {
    b->next->prev = a;
  }

  b->next = tempNext;
  b->prev = tempPrev;
}

LinkedList* linkedList_create()
{
  LinkedList* list = (LinkedList*)malloc(sizeof(LinkedList));
  list->head = NULL;
  list->tail = NULL;
  list->size = 0;
}

void linkedList_delete(LinkedList* list)
{
  if (!list->head || !list->tail)
  {
    return;
  }

  LinkedListNode* current = list->tail;
  while (current != NULL)
  {
    LinkedListNode* node = current;
    current = current->prev;
    free(node);
  }

  free(list);
  list = NULL;
}

void linkedList_modify(LinkedList* list, int index, int newValue)
{
  if (index >= list->size)
  {
    return;
  }

  LinkedListNode* current = list->head;

  for (int i = 0; i < index; i++)
  {
    current = current->next;
  }

  current->data = newValue;
}

void linkedList_append(LinkedList* list, int data)
{
  list->size++;
  LinkedListNode* newNode = linkedListNode_create(data);

  if (!list->head)
  {
    list->head = newNode;
    list->tail = newNode;
    return;
  }

  list->tail->next = newNode;
  newNode->prev = list->tail;
  list->tail = newNode;
}

void linkedList_pop(LinkedList* list)
{
  if (!list->head || !list->tail || list->size == 0)
  {
    return;
  }

  list->size--;

  if (list->head == list->tail)
  {
    free(list->head);
    list->head = NULL;
    list->tail = NULL;
    return;
  }

  LinkedListNode* prev = list->tail->prev;
  prev->next = NULL;
  free(list->tail);
  list->tail = prev;
}

LinkedList* linkedList_copy(LinkedList* list)
{
  LinkedList* newList = linkedList_create();

  if (list->size == 0)
  {
    return newList;
  }

  LinkedListNode* current = list->head;

  while (current != NULL)
  {
    linkedList_append(newList, current->data);
    current = current->next;
  }

  return newList;
}

LinkedList* linkedList_reverse(LinkedList* list)
{
  LinkedList* newList = linkedList_create();

  if (list->size == 0)
  {
    return newList;
  }

  LinkedListNode* current = list->tail;

  while (current != NULL)
  {
    linkedList_append(newList, current->data);
    current = current->prev;
  }

  return newList;
}

void linkedList_traverse(LinkedList* list, void (*fn)(LinkedListNode*))
{
  LinkedListNode* current = list->head;

  while (current != NULL)
  {
    fn(current);
    current = current->next;
  }
}

void linkedList_traverseReverse(LinkedList* list, void (*fn)(LinkedListNode*))
{
  LinkedListNode* current = list->tail;

  while (current != NULL)
  {
    fn(current);
    current = current->prev;
  }
}

void linkedList_print(LinkedList* list)
{
  if (list->size == 0)
  {
    return;
  }

  linkedList_traverse(list, linkedListNode_print);

  printf("\n");
}

void linkedList_printReverse(LinkedList* list)
{
  if (list->size == 0)
  {
    return;
  }

  linkedList_traverseReverse(list, linkedListNode_print);

  printf("\n");
}

void linkedList_appendTo(LinkedList* list, int index, int data)
{
  if (index >= list->size)
  {
    return;
  }

  list->size++;

  LinkedListNode* newNode = linkedListNode_create(data);
  LinkedListNode* current = list->head;

  for (int i = 0; i < index; i++)
  {
    current = current->next;
  }

  newNode->next = current->next;
  if (current->next)
  {
    current->next->prev = newNode;
  }

  current->next = newNode;
  newNode->prev = current;

  if (current == list->tail)
  {
    list->tail = newNode;
  }
}

void linkedList_popFrom(LinkedList* list, int index)
{
  if (index >= list->size)
  {
    return;
  }

  list->size--;

  if (index == 0)
  {
    if (list->head == list->tail)
    {
      free(list->head);
      list->head = NULL;
      list->tail = NULL;
      return;
    }

    LinkedListNode* head = list->head;
    list->head = list->head->next;

    if (list->head != NULL)
    {
      list->head->prev = NULL;
    }

    free(head);
    return;
  }

  if (index == list->size)
  {
    if (list->head == list->tail)
    {
      free(list->head);
      list->head = NULL;
      list->tail = NULL;
      return;
    }

    LinkedListNode* tail = list->tail;
    list->tail = list->tail->prev;

    if (list->tail != NULL)
    {
      list->tail->next = NULL;
    }

    free(tail);
    return;
  }

  LinkedListNode* current = list->head;

  for (int i = 0; i < index; i++)
  {
    current = current->next;
  }

  current->next->prev = current->prev;
  current->prev->next = current->next;
  free(current);
}

void linkedList_sort(LinkedList* list)
{
  struct LinkedListNode* node = NULL, * temp = NULL;
  int tempvar;
  node = list->head;
  temp = node;

  while (node != NULL)
  {
    temp = node;
    while (temp->next != NULL)
    {
      if (temp->data > temp->next->data)
      {
        tempvar = temp->data;
        temp->data = temp->next->data;
        temp->next->data = tempvar;
      }
      temp = temp->next;
    }
    node = node->next;
  }
}

BOOL linkedList_hasValue(LinkedList* list, int value)
{
  LinkedListNode* current = list->head;

  while (current != NULL)
  {
    if (current->data == value)
    {
      return TRUE;
    }
    current = current->next;
  }

  return FALSE;
}

LinkedList* linkedList_join(LinkedList* a, LinkedList* b)
{
  LinkedList* joined = linkedList_create();
  joined->size = a->size + b->size;

  LinkedListNode* current = a->head;

  while (current != NULL)
  {
    linkedList_append(joined, current->data);
    current = current->next;
  }

  current = b->head;
  while (current != NULL)
  {
    linkedList_append(joined, current->data);
    current = current->next;
  }

  return joined;
}



#endif
